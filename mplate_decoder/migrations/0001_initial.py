# Generated by Django 2.2.3 on 2019-08-16 22:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lacquer_code', models.CharField(max_length=30)),
                ('color_name', models.CharField(max_length=75)),
                ('ral_code', models.CharField(blank=True, max_length=30)),
                ('chip', models.CharField(blank=True, max_length=36)),
            ],
        ),
        migrations.CreateModel(
            name='Engine',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('engine_code', models.PositiveSmallIntegerField()),
                ('engine_type', models.CharField(max_length=6)),
                ('fuel_induction', models.CharField(max_length=30)),
                ('extra_specs', models.CharField(blank=True, max_length=50)),
                ('m_codes', models.CharField(blank=True, help_text='List of M-codes for the corresponding extras', max_length=50)),
                ('years', models.CharField(blank=True, max_length=65)),
            ],
        ),
        migrations.CreateModel(
            name='ExportDestination',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('export_code', models.CharField(max_length=3)),
                ('export_destination', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='ExteriorColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plate_code', models.CharField(max_length=4)),
                ('lacquer_code_body', models.CharField(max_length=20)),
                ('lacquer_code_roof', models.CharField(blank=True, max_length=20)),
                ('years', models.CharField(blank=True, max_length=65)),
                ('sonderlackierung', models.BooleanField()),
                ('remarks', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Gearbox',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gearbox_code', models.PositiveSmallIntegerField()),
                ('gearbox_description', models.CharField(max_length=35)),
                ('m_codes', models.CharField(blank=True, help_text='List of M-codes for the corresponding extras', max_length=50)),
                ('years', models.CharField(blank=True, max_length=65)),
            ],
        ),
        migrations.CreateModel(
            name='InteriorColor',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('plate_code', models.CharField(max_length=2)),
                ('color_name', models.CharField(max_length=50)),
                ('material', models.CharField(max_length=20)),
                ('years', models.CharField(blank=True, max_length=65)),
                ('remarks', models.TextField(blank=True)),
                ('image', models.ImageField(blank=True, upload_to='')),
            ],
        ),
        migrations.CreateModel(
            name='Mcode',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('m_code', models.CharField(max_length=3)),
                ('description', models.TextField()),
                ('years', models.CharField(blank=True, max_length=65)),
                ('remarks', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='McodeCollection',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('m_code', models.CharField(max_length=3)),
                ('collection', models.TextField()),
                ('years', models.CharField(blank=True, max_length=65)),
                ('remarks', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Mplate',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('chassis_number_short', models.CharField(help_text='Chassis number shortened, with the two leading digits removed.', max_length=8, unique=True)),
                ('m_codes_1', models.CharField(blank=True, help_text='Row 1 of M codes (max 5)', max_length=19)),
                ('m_codes_2', models.CharField(help_text="Row 2 of M codes (max 4 -mod. '70-'79 or 5 -mod. '68-'69-)", max_length=19)),
                ('paint_and_interior', models.CharField(help_text='Combined VW body/roof paint and interior codes', max_length=6)),
                ('production_date', models.CharField(help_text='Production date', max_length=3)),
                ('production_planned', models.CharField(blank=True, help_text='Code used for production planning', max_length=4)),
                ('export_destination', models.CharField(blank=True, help_text='Destination code', max_length=3)),
                ('model', models.CharField(help_text='Vehicle model', max_length=4)),
                ('aggregate_code', models.CharField(help_text='Engine and gearbox codes', max_length=2)),
                ('emden', models.CharField(blank=True, help_text='Optional "E" for\n            Emden', max_length=1)),
            ],
        ),
        migrations.CreateModel(
            name='Type2Model',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.PositiveSmallIntegerField()),
                ('description', models.CharField(help_text='Model description', max_length=35)),
                ('schematic', models.TextField(blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Type2ModelConfiguration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.PositiveSmallIntegerField()),
                ('configuration', models.PositiveSmallIntegerField()),
                ('description', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Type2ModelExtra',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('model', models.PositiveSmallIntegerField()),
                ('extras', models.PositiveSmallIntegerField()),
                ('description', models.TextField()),
                ('m_codes', models.CharField(blank=True, help_text='List of M-codes for the corresponding extras', max_length=50)),
                ('chassis_plate', models.CharField(blank=True, help_text='Model description as it appears on the chassis plate', max_length=20)),
                ('years', models.CharField(blank=True, max_length=65)),
            ],
        ),
    ]
